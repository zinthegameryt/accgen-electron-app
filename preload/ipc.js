var ipc = require("electron").ipcRenderer;

function send(event) {
  for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    args[_key - 1] = arguments[_key];
  }

  event = "accgen-web-" + event;
  ipc.send.apply(ipc, [event].concat(args));
}

function on(event, callback) {
  event = "accgen-web-" + event;
  ipc.on(event, callback);
}

exports.send = send;
exports.on = on;