// If steam, we should load the steam preload script that will replace the page with the captcha.
if (window.location.href == "https://store.steampowered.com/join/") {
    // Executes on load
    require("./steam.js");
    return;
}

const isDev = require("electron-is-dev");
if (!isDev && !window.location.hostname.endsWith("cathook.club"))
    return;

const proxyAgent = require('proxy-agent');
// Full IPC
const ipc = require("electron").ipcRenderer;
const URL = require("url");
const nodefetch = require("node-fetch");

function getProxiedFetch(proxy) {
    return function (url, init) {
        var agent = undefined;
        if (proxy) {
            var proxyopts = URL.parse(proxy);
            if (proxyopts.protocol == "https:")
                proxyopts.protocol = "http:"
            agent = new proxyAgent(proxyopts);
        }
        return Promise.race([
            nodefetch(url, extend({
                agent: agent
            }, init)),
            new Promise((_, reject) =>
                setTimeout(() => reject(new Error('timeout')), init.timeout || 15000)
            )
        ]);
    }
}

function startSteam(account) {
    ipc.send("start-steam", account);
}

document.sagelectron = {
    ipc: require("./ipc.js"),
    isDev: isDev,
    startSteam: startSteam,
    getProxiedFetch: getProxiedFetch,
    nodefetch: nodefetch,
    apiversion: 5
}
